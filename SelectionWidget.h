#ifndef SELECTIONWIDGET_H
#define SELECTIONWIDGET_H

#include <QWidget>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QStandardPaths>
#include <QDebug>
#include <QFileInfoList>
#include <QProcess>

#include <QVBoxLayout>
#include <QRadioButton>
#include <QMessageBox>
#include <QPushButton>

#include <QDateTime>

#include <QHash>

QT_BEGIN_NAMESPACE
namespace Ui
{
class SelectionWidget;
}
QT_END_NAMESPACE

class SelectionWidget : public QWidget
{
	Q_OBJECT

public:
	SelectionWidget (QWidget *parent = nullptr);
	~SelectionWidget();

private slots:

	void on_pB_Exit_clicked ();

	void on_pB_AddProfile_clicked ();

	void on_pB_ShowTextBrowser_clicked ();

private:
	Ui::SelectionWidget *ui;

	QDir *wastelandDir;
	QStringList profilesList;
	QFileInfo dirInfoGdsd;		  // For Game's default savegame directory

	QList<QRadioButton *> selRbList;

	void refreshProfilesUI (QLayout *layout, QFileInfoList dirsList = QFileInfoList{});
	void addProfileToUI (QLayout *layout, const QString &profileName, int index = -1);
	void addPlayGameButton ();
	void runGameAndExit ();

	bool removePreviousLink ();
	bool makeLink (QString profileName);

	void setCurrentProfile (int profileIndexFromProfilesList);

	bool firstTimeConfiguration (QDir &wlDir);

	bool currentlySetLinkIsFine = false;
	bool infoVisible = false;

	QString wl3DF_IntendedUsage{};
	QString iconLocation{};
	QString execFileLocation{};
	QString appRunPath{};
	QString desktopFileLocation{};
};
#endif		  // SELECTIONWIDGET_H
