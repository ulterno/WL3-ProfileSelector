# WL3 - Profile Selector (For Wasteland 3 GoG version)

Expected Saved Game directory : `~/.config/Wasteland3/Saved Games`

### To use this Switcher

1.  When you start the program for the first time, it will ask you if you want it to set up the saved games directory to work with it.
    - Using this application requires write privileges to the Wasteland 3 Saved Game directory.
2.  Start the program as your user and select the Profile's radio button
3.  To make more profiles, you can Add more folders to the directory `~/.config/Wasteland3/` matching the format and select them in the Switcher.
    - Or you may use the "Add Profile" button in the program.

### License

-   GPLv3
