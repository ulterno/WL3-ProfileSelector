#include "SelectionWidget.h"
#include "ui_SelectionWidget.h"

constexpr char game_default_savegame_dirname[] = "Save Games";
constexpr int max_profiles = 20;
constexpr char timestamp_colour[] = "Teal";
constexpr char profilename_prefix[] = "Profile ";
constexpr char config_filename[] = "ProfileSelectorConfig";

QString timeStampString (bool forDebug = false)
{
	if (forDebug)
	{
		return QDateTime::currentDateTime().toString ("[yyyy-MM-dd hh:mm:ss.zzz] ");
	}
	return QString ("<span style=\"color:" + QString (timestamp_colour) + "\">" + QDateTime::currentDateTime().toString ("[yyyy-MM-dd hh:mm:ss.zzz] ")
		+ "</span>");
}

int customIndexOf (const QFileInfoList &list, const QString &absoluteFilePath)
{
	for (int iter = 0; iter < list.length(); ++iter)
	{
		if (list[iter].absoluteFilePath() == absoluteFilePath)
		{
			return int (iter);
		}
	}
	return -1;
}

SelectionWidget::SelectionWidget (QWidget *parent) : QWidget (parent), ui (new Ui::SelectionWidget)
{
	///	Setup UI
	ui->setupUi (this);
	setWindowTitle ("Wasteland 3 Profile Selection");
	resize (QSize (300, 180));
	ui->infoDisp->setVisible (infoVisible);

	///	Open Wasteland3 config location
	QString usedDir = QStandardPaths::writableLocation (QStandardPaths::GenericConfigLocation) + QStringLiteral ("/Wasteland3");
	if (usedDir == QStringLiteral ("/Wasteland3"))
	{
		ui->infoDisp->append (timeStampString()
			+ +"Save game location not found: \n"
			   "The directory for Wasteland3 saved games is unavailable or is in the wrong location or has wrong permissions. Please check it out "
			   "yourself.");
		QMessageBox::critical (this, "Save game location not found",
			"The directory for Wasteland3 saved games is unavailable or is in the wrong location or has wrong permissions. Please check it out "
			"yourself.");
		exit (11);
	}
	qDebug() << "Going into" << usedDir;
	wastelandDir = new QDir (usedDir);
	ui->infoDisp->append (timeStampString() + "Directory : " + wastelandDir->absolutePath());
	///	Check all currently available files and subdirs and report if program can work
	QFileInfoList allDirInfo = wastelandDir->entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot);
	qDebug() << "Detected Dirs" << allDirInfo;
	bool dirInfoGdsdPresent = false;
	int indexOfGDSD = customIndexOf (allDirInfo, wastelandDir->absoluteFilePath (game_default_savegame_dirname));
	if (indexOfGDSD > -1)
	{
		dirInfoGdsd = allDirInfo.takeAt (indexOfGDSD);
		dirInfoGdsdPresent = true;
	}
	else
	{
		dirInfoGdsd = QFileInfo{};
	}
	if (dirInfoGdsdPresent)
	{
		if (!dirInfoGdsd.isSymLink())
		{
			qWarning() << "WARNING : " + QString (game_default_savegame_dirname) + " is an Actual Directory (not a symlink)";
			QMessageBox::StandardButton userRet = QMessageBox::question (this, "Invalid System Configuration",
				"WARNING : " + QString (game_default_savegame_dirname) + " is an Actual Directory (not a symlink). Continue ?");
			ui->infoDisp->append (timeStampString() + "WARNING : Invalid System Configuration: " + QString (game_default_savegame_dirname)
				+ " is an Actual Directory (not a symlink). Continue ?");
			if ((userRet & (QMessageBox::Ok | QMessageBox::Yes)) != 0)
			{
				if (!firstTimeConfiguration (*wastelandDir))
				{
					exit (0);
				}
			}
			else
			{
				exit (0);
			}
		}
		else
		{
			currentlySetLinkIsFine = true;
		}
	}
	else		// !dirInfoGdsdPresent
	{
		// "Save Games" Folder/Link Not found
		qWarning() << "WARNING: No folder / SymLink to folder named " + QString (game_default_savegame_dirname) + " in " + usedDir;
		QMessageBox::warning (
			this, "WARNING", "WARNING: No folder / SymLink to folder named " + QString (game_default_savegame_dirname) + " in " + usedDir);
		ui->infoDisp->append (
			timeStampString() + +"WARNING: No folder / SymLink to folder named " + QString (game_default_savegame_dirname) + " in " + usedDir);
		QFileInfoList allFileInfo = wastelandDir->entryInfoList (QDir::Files);
		QFileInfo fileInfoGdsd;
		bool fileInfoGdsdPresent = false;
		int indexOfGDSDf = customIndexOf (allFileInfo, wastelandDir->absoluteFilePath (game_default_savegame_dirname));
		if (indexOfGDSDf > -1)
		{
			fileInfoGdsd = allDirInfo.takeAt (indexOfGDSDf);
			fileInfoGdsdPresent = true;
		}
		if (fileInfoGdsdPresent)
		{
			if (fileInfoGdsd.isSymLink())
			{
				// "Save Games" Links to a file
				qWarning() << "WARNING: SymLink named " + QString (game_default_savegame_dirname) + " in " + usedDir + " refers to a File";
				QMessageBox::warning (
					this, "WARNING", "WARNING: SymLink named " + QString (game_default_savegame_dirname) + " in " + usedDir + " refers to a File");
				ui->infoDisp->append (
					timeStampString() + "WARNING: SymLink named " + QString (game_default_savegame_dirname) + " in " + usedDir + " refers to a File");
			}
			else
			{
				// "Save Games" IS a FILE
				qWarning() << "WARNING: FILE named " + QString (game_default_savegame_dirname) + " exists in " + usedDir;
				QMessageBox::warning (this, "WARNING", "WARNING: FILE named " + QString (game_default_savegame_dirname) + " exists in " + usedDir);
				ui->infoDisp->append (
					timeStampString() + +"WARNING: FILE named " + QString (game_default_savegame_dirname) + " exists in " + usedDir);
			}
		}
	}

	///	Show list of profiles in UI
	qDebug() << "Making Profiles List";
	QVBoxLayout *boxOfProfileSelButtons = new QVBoxLayout;
	refreshProfilesUI (boxOfProfileSelButtons, allDirInfo);
	qDebug() << profilesList;
	ui->infoDisp->append (timeStampString() + "Detected profiles:");
	for (const QString &pName : qAsConst (profilesList))
	{
		ui->infoDisp->append ("	" + pName);
	}
	ui->gB_SetStuff->setLayout (boxOfProfileSelButtons);

	///	Select the radio button corresponding to currently active Profile
	if (currentlySetLinkIsFine)
	{
		int IndexOfCurrentProfile = profilesList.indexOf (QFileInfo (dirInfoGdsd.symLinkTarget()).fileName());
		if (IndexOfCurrentProfile > -1)
		{
			selRbList.at (IndexOfCurrentProfile)->setChecked (true);
			ui->infoDisp->append (timeStampString() + QFileInfo (dirInfoGdsd.symLinkTarget()).fileName() + " is the currently selected profile");
		}
		else
		{
			qDebug() << "SymLinkTarget : " << dirInfoGdsd.symLinkTarget();
			ui->infoDisp->append (timeStampString()
				+ "Current profile target unusable. "
				  "SymLinkTarget : "
				+ dirInfoGdsd.symLinkTarget());
		}
	}
	addPlayGameButton();
}

SelectionWidget::~SelectionWidget()
{
	delete ui;
	delete wastelandDir;
}

bool SelectionWidget::removePreviousLink()
{
	currentlySetLinkIsFine = false;
	if (!QDir (wastelandDir->absoluteFilePath (game_default_savegame_dirname)).exists())
	{
		ui->infoDisp->append (timeStampString() + "No link to profile exists");
		return true;
	}
	ui->infoDisp->append (timeStampString() + "Removing... " + wastelandDir->absoluteFilePath (game_default_savegame_dirname));
	return QFile::remove (wastelandDir->absoluteFilePath (game_default_savegame_dirname));
}

bool SelectionWidget::makeLink (QString profileName)
{
	ui->infoDisp->append (timeStampString() + "Creating Link to selected profile...");
	if (profilesList.contains (profileName))
	{
		QFile lf (wastelandDir->absoluteFilePath (profileName));
		if (lf.link (wastelandDir->absoluteFilePath (profileName), wastelandDir->absoluteFilePath (game_default_savegame_dirname)))
		{
			// Success
			ui->infoDisp->append (timeStampString() + "Created link to profile: " + wastelandDir->absoluteFilePath (profileName));
			qDebug() << "Done."
					 << "Now you can go and plaaaaaaay";
			currentlySetLinkIsFine = true;
			return true;
		}
		else
		{
			ui->infoDisp->append (timeStampString() + "Unable to make Link : " + lf.errorString());
			qDebug() << "Unable to make Link : " << lf.error();
			currentlySetLinkIsFine = false;
			return false;
		}
	}
	else
	{
		ui->infoDisp->append (timeStampString() + "Invalid Profile name");
		currentlySetLinkIsFine = false;
		return false;
	}
}

void SelectionWidget::setCurrentProfile (int profileIndexFromProfilesList)
{
	if (removePreviousLink())
	{
		if (profilesList.length() > profileIndexFromProfilesList)
		{
			if (makeLink (profilesList.at (profileIndexFromProfilesList)))
			{
				currentlySetLinkIsFine = true;
				return;
			}
			else
			{
				currentlySetLinkIsFine = false;
				QMessageBox::information (this, "Profile change failed", "Unable to make SymLink");
			}
		}
		else
		{
			QMessageBox::information (this, "Profile change failed", "Programmer did something bad. Ask them");
			Q_ASSERT (false);
			currentlySetLinkIsFine = false;
		}
	}
	else
	{
		QMessageBox::information (this, "Profile change failed", "Unable to remove previous link");
		currentlySetLinkIsFine = false;
	}
}

bool SelectionWidget::firstTimeConfiguration (QDir &wlDir)
{
	QMessageBox::StandardButton userRet = QMessageBox::question (this, "First time configuration",
		"Since the configuration in \"" + wlDir.absoluteFilePath (game_default_savegame_dirname)
			+ "\" looks like it has not been initialised for this program, the first time configuration will be run.\nIf you think this is a "
			  "mistake, please cancel (Click No) and check the location yourself.\n\n\tContinue?");
	if ((userRet & (QMessageBox::Ok | QMessageBox::Yes)) != 0)
	{
		// First time configuration
		ui->infoDisp->append (timeStampString() + "Commencing first time configuration...");
		QString newName = QString (profilename_prefix) + "0";
		if (wlDir.entryList().contains (game_default_savegame_dirname))
		{
			if (!QFileInfo::exists (wlDir.absoluteFilePath (newName)))
			{
				if (!wlDir.rename (wlDir.absoluteFilePath (game_default_savegame_dirname), newName))
				{
					ui->infoDisp->append (timeStampString() + "Failed renaming directory \"" + wlDir.absoluteFilePath (game_default_savegame_dirname)
						+ "\". Please check availability.");
					qWarning() << timeStampString (true) + "Failed renaming directory \"" + wlDir.absoluteFilePath (game_default_savegame_dirname)
							+ "\". Please check availability.";
				}
			}
			else
			{
				if (!QDir (wlDir.absoluteFilePath (newName)).entryList (QDir::AllEntries | QDir::NoDotAndDotDot).isEmpty())
				{
					ui->infoDisp->append (timeStampString() + "Failed renaming directory \"" + wlDir.absoluteFilePath (game_default_savegame_dirname)
						+ "\" : \"" + newName + "\" exists.");
					qWarning() << timeStampString (true) + "Failed renaming directory \"" + wlDir.absoluteFilePath (game_default_savegame_dirname)
							+ "\" : \"" + newName + "\" exists.";
				}
				else
				{
					if (!QDir (wlDir.absoluteFilePath (newName)).removeRecursively())
					{
						ui->infoDisp->append (timeStampString() + "Failed renaming directory \""
							+ wlDir.absoluteFilePath (game_default_savegame_dirname) + "\" : Unable to remove \"" + newName + "\".");
						qWarning() << timeStampString (true) + "Failed renaming directory \"" + wlDir.absoluteFilePath (game_default_savegame_dirname)
								+ "\" : Unable to remove \"" + newName + "\".";
					}
					else
					{
						if (!wlDir.rename (wlDir.absoluteFilePath (game_default_savegame_dirname), newName))
						{
							ui->infoDisp->append (timeStampString() + "Failed renaming directory \""
								+ wlDir.absoluteFilePath (game_default_savegame_dirname) + "\". Please check availability.");
							qWarning() << timeStampString (true) + "Failed renaming directory \""
									+ wlDir.absoluteFilePath (game_default_savegame_dirname) + "\". Please check availability.";
						}
					}
				}
			}
		}
		else
		{
			if (!wlDir.mkdir (newName))
			{
				ui->infoDisp->append (timeStampString() + "Failed creating directory \"" + newName + "\". Please check availability.");
			}
		}
		return true;
	}
	else
	{
		return false;
	}
}

void SelectionWidget::on_pB_Exit_clicked() { exit (0); }

void SelectionWidget::on_pB_AddProfile_clicked()
{
	QString pPref (profilename_prefix);
	int pIndex;
	for (pIndex = 1; pIndex < max_profiles; pIndex++)
	{
		if (!profilesList.contains (pPref + QString::number (pIndex)))
		{
			break;
		}
	}
	if (wastelandDir->mkdir (pPref + QString::number (pIndex)))
	{
		ui->infoDisp->append (timeStampString() + "New profile created: " + pPref + QString::number (pIndex));
		addProfileToUI (ui->gB_SetStuff->layout(), pPref + QString::number (pIndex));
	}
	else
	{
		QMessageBox::information (this, "Error Creating Profile",
			"Unable to create directory: " + pPref + QString::number (pIndex) + " Please check permissions and stuff.");
		ui->infoDisp->append (timeStampString()
			+ "Error Creating Profile: "
			  "Unable to create directory: "
			+ pPref + QString::number (pIndex));
	}
}

void SelectionWidget::addProfileToUI (QLayout *layout, const QString &profileName, int index)
{
	if (index < 0)
	{
		index = profilesList.length() + 0;
	}
	profilesList << profileName;
	QRadioButton *arb = new QRadioButton (profileName);
	layout->addWidget (arb);
	selRbList << arb;
	connect (arb, &QRadioButton::toggled, this,
		[this, index] (bool checked)
		{
		if (checked)
		{
			setCurrentProfile (index);
		}
	});
}

void SelectionWidget::addPlayGameButton()
{
	///	Check if button will work on making
	QFile configFile (QStandardPaths::writableLocation (QStandardPaths::AppConfigLocation) + "/" + config_filename);
	if (configFile.open (QFile::ReadOnly))
	{
		// Config file present. Use it
		QTextStream confStream (&configFile);
		while (!confStream.atEnd())
		{
			QStringList line (confStream.readLine().split ("="));
			if (!line.isEmpty())
			{
				if (line.at (0) == "Wasteland3Icon" && line.length() > 1)
				{
					iconLocation = line.at (1);
				}
				else if (line.at (0) == "Wasteland3ExecFile" && line.length() > 1)
				{
					execFileLocation = line.at (1);
				}
				else if (line.at (0) == "Wasteland3AppRunPath" && line.length() > 1)
				{
					appRunPath = line.at (1);
				}
			}
		}
		configFile.close();
		if ((execFileLocation.isEmpty() || appRunPath.isEmpty()) && iconLocation.isEmpty())
		{
			// Config file useless. Remove it and retry
			configFile.remove();
			return addPlayGameButton();
		}
	}
	else
	{
		// No config file. Try and make one
		QString dirPath (QStandardPaths::writableLocation (QStandardPaths::ApplicationsLocation));
		qDebug() << "Checking Directory " << dirPath;
		ui->infoDisp->append (timeStampString() + "Checking Directory " + dirPath + " for Wasteland 3 desktop file");
		QFileInfo appliDirInfo (dirPath);
		if (!appliDirInfo.isDir())
		{
			return;
		}
		// ~/.local/share/applications/ is an actual Directory
		QFileInfoList dFiles = QDir (dirPath).entryInfoList (QDir::Files | QDir::NoDotAndDotDot);
		for (const QFileInfo &aFile : qAsConst (dFiles))
		{
			if (aFile.baseName().contains ("Wasteland") && aFile.baseName().contains ("3") && (aFile.suffix() == "desktop"))
			{
				// You got the file you are looking for (maybe)
				QFile wl3DesktopFile (aFile.absoluteFilePath());
				if (!wl3DesktopFile.open (QFile::ReadOnly))
				{
					ui->infoDisp->append (timeStampString() + "Unable to open desktop file " + aFile.absoluteFilePath());
					return;
				}
				QTextStream dfStream (&wl3DesktopFile);
				QHash<QString, QString> confVals;
				while (!dfStream.atEnd())
				{
					QString line = dfStream.readLine();
					if (line.isEmpty())
					{
						continue;
					}
					if (line.trimmed().at (0) == '#')
					{
						if (line.trimmed().at (1) == "!")
						{
							wl3DF_IntendedUsage = line.right (line.length() - line.indexOf ("!"));
						}
						continue;
					}
					if (line == "[Desktop Entry]")
					{
						continue;
					}
					QStringList spList = line.split ("=");
					if (spList.isEmpty())
					{
						continue;
					}
					if (spList.length() < 2)
					{
						continue;
					}
					confVals.insert (spList.at (0), spList.at (1));
				}
				wl3DesktopFile.close();
				if (confVals.value ("Name") == "Wasteland 3" && confVals.value ("Type") == "Application")
				{
					execFileLocation = confVals.value ("Exec");
					iconLocation = confVals.value ("Icon");
					appRunPath = confVals.value ("Path");
					qDebug() << ".desktop file configs: " << confVals;
					QString configPath = QStandardPaths::writableLocation (QStandardPaths::AppConfigLocation);
					if (QDir (QStandardPaths::writableLocation (QStandardPaths::AppConfigLocation)).mkpath ("."))
					{
						if (configFile.open (QFile::WriteOnly))
						{
							QTextStream cWrite (&configFile);
							cWrite << "Wasteland3Icon=" << iconLocation << "\n";
							cWrite << "Wasteland3ExecFile=" << execFileLocation << "\n";
							cWrite << "Wasteland3DesktopFile=" << aFile.absoluteFilePath() << "\n";
							cWrite << "Wasteland3AppRunPath=" << appRunPath << "\n";
							if (!configFile.flush())
							{
								qDebug() << "Difficulty writing to file: " << configFile.fileName();
								ui->infoDisp->append (timeStampString() + "Difficulty writing to file: " + configFile.fileName());
							}
							else
							{
								ui->infoDisp->append (timeStampString() + "Writing Config to file: " + configFile.fileName());
							}
							configFile.close();
						}
						else
						{
							ui->infoDisp->append (timeStampString() + "Unable to make config file: " + configFile.fileName());
						}
					}
					else
					{
						ui->infoDisp->append (timeStampString() + "Unable to access directory for config file: " + configPath);
					}
				}
				// You found the file. Now stop looking
				break;
			}
		}
	}
	if ((!execFileLocation.isEmpty()) && (!iconLocation.isEmpty()))
	{
		///	Make button
		QPushButton *runButton = new QPushButton (QIcon (iconLocation), "Start Wasteland 3");
		ui->hL_FinalButtons->addWidget (runButton);
		connect (runButton, &QPushButton::clicked, this, &SelectionWidget::runGameAndExit);
	}
}

void SelectionWidget::runGameAndExit()
{
	///	Start Program
	QString command = execFileLocation.remove ("\"").trimmed();
	QStringList arguments{};
	if (QProcess::startDetached (command, arguments, appRunPath))
	{
		///	Exit after success
		exit (0);
	}
	else
	{
		QString argstring;
		for (const QString &s : qAsConst (arguments))
		{
			argstring.append (" " + s);
		}
		ui->infoDisp->append (timeStampString() + "Unable to start program: " + command + " " + argstring + " in " + appRunPath);
	}
}

void SelectionWidget::on_pB_ShowTextBrowser_clicked()
{
	infoVisible = !infoVisible;
	ui->infoDisp->setVisible (infoVisible);
	if (infoVisible)
	{
		ui->infoDisp->append (timeStampString() + "Showing Information Display");
		ui->pB_ShowTextBrowser->setText ("<");
		resize (QSize (size().width() + 400, size().height()));
	}
	else
	{
		ui->infoDisp->append (timeStampString() + "Hiding Information Display");
		ui->pB_ShowTextBrowser->setText (">");
		resize (QSize (size().width() - 400, size().height()));
	}
}

void SelectionWidget::refreshProfilesUI (QLayout *layout, QFileInfoList dirsList)
{
	if (dirsList.isEmpty())
	{
		dirsList = wastelandDir->entryInfoList (QDir::Dirs | QDir::NoDotAndDotDot);
	}
	// Empty layout
	QLayoutItem *child;
	while ((child = layout->takeAt (0)) != nullptr)
	{
		delete child->widget();		   // delete the widget
		delete child;				   // delete the layout item
	}
	// Empty Profiles List
	profilesList.clear();
	// Empty List of stored Radio Button pointers
	selRbList.clear();
	// Repopulate layout
	int profileIndex = 0;
	for (const QFileInfo &fileInfo : qAsConst (dirsList))
	{
		if (fileInfo.fileName() != QString (game_default_savegame_dirname))
		{
			qDebug() << "	" << fileInfo.filePath();
			if (fileInfo.fileName().left (8) == profilename_prefix)
			{
				// Note: Only the directory names need to match the format.
				// They might actualy be SymLinks to another location
				// This way you can keep your stuff somewhere else
				addProfileToUI (layout, fileInfo.fileName(), profileIndex);
				profileIndex++;
			}
		}
	}
}
